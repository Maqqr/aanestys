import io
from operator import itemgetter
from flask import Flask, request, jsonify, make_response
import dataset

app = Flask(__name__)

DB_PATH = 'sqlite:////var/www/aanestys/vote.db'
WRITE_PATH = '/var/www/aanestys/results.csv'

#DB_PATH = 'sqlite:///vote.db'
#WRITE_PATH = 'results.csv'


@app.route('/_submit')
def submit():
    name = request.args.get('name', 'default')
    vote = request.args.get('vote', 'default')
    ip = request.remote_addr

    with dataset.connect(DB_PATH) as db:
        votetable = db['vote']
        votetable.insert(dict(name=name, vote=vote, ip=ip))

    return jsonify(result="ok")


def get_csv():
    page = io.StringIO()
    with dataset.connect(DB_PATH) as db:
        votetable = db['vote']
        for vote in votetable:
            page.write(vote['ip'] + '#' + vote['name'] + '#' + vote['vote'] + '\n')
    return page.getvalue()


@app.route("/_writecsv")
def writecsv():
    csv = get_csv()
    with open(WRITE_PATH, 'w') as handle:
        handle.write(csv.encode('utf-8'))
    return 'done', 200


@app.route("/_resultscsv")
def resultscsv():
    page = get_csv()
    output = make_response(page)
    output.headers["Content-Disposition"] = "attachment; filename=export.csv"
    output.headers["Content-type"] = "text/csv"
    return output


def calc_scores(games):
    scores = {}
    for i, game in enumerate(games):
        scores[game] = (len(games) - i) / float(len(games))
    return scores


@app.route("/_resultss")
def results():
    res = {}
    csv = get_csv()
    for vote in [line.strip() for line in csv.splitlines()]:
        games = vote.split('#')[2].split(';')
        for game, score in calc_scores(games).items():
            if game in res:
                res[game] += score
            else:
                res[game] = score

    lista = []
    for game, score in res.items():
        lista.append((score, game))

    lista.sort(key=itemgetter(0), reverse=True)
    return '<br/>'.join(map(repr, lista))


@app.route("/_results/names")
def resultsnames():
    res = []
    with dataset.connect(DB_PATH) as db:
        votetable = db['vote']
        for vote in votetable:
            name = vote['name']
            res.append(name)
    res.insert(0, "Aania: " + str(len(res)))
    return '<br>'.join(res)


@app.route('/')
def hello_world():
    return ''
