#!/usr/bin/python
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/aanestys/")

from Aanestys.aanestys import app as application
application.secret_key = 'secretkey'
