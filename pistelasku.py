from operator import itemgetter

def calculate_scores(games):
    scores = {}
    for i, game in enumerate(games):
        scores[game] = (len(games) - i) / float(len(games))
    return scores

def main():
    res = {}
    with open('results.csv') as exportfile:
        content = exportfile.read().decode('utf-8')
        for vote in [line.strip() for line in content.splitlines()]:
            games = vote.split('#')[2].split(';')
            for game, score in calculate_scores(games).items():
                if game in res:
                    res[game] += score
                else:
                    res[game] = score

    lista = []
    for game, score in res.items():
        lista.append((score, game))

    lista.sort(key=itemgetter(0), reverse=True)
    print('\n'.join(map(repr, lista)))


if __name__ == '__main__':
    main()
