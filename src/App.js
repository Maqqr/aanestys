import Sortable from 'sortablejs';
import React, { Component } from 'react';
import $ from 'jquery';
require('featherlight');
require('isomorphic-fetch');
const gsheets = require('gsheets');


class Game extends Component {
  render() {
    return (
      <li className="list-group-item" data-key={this.props.keyProp} style={{padding: 0, margin: 0}}>
        <div className="row">
            <div className="col-md-1 col-xs-3 col-sm-2">
                <button className="btn btn-link" data-featherlight={this.props.imgSrc}><img width="60" height="45" alt="" src={this.props.imgSrc}/></button>
            </div>
            <div className="col-md-10 col-xs-9 col-sm-10">
                <h4>{this.props.name} <small> &mdash; {this.props.creator}</small></h4>
            </div>
        </div>
    </li>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { games: [], voted: false };
  }

  componentDidMount() {
    var gamelist = document.getElementById("gamelist");
    Sortable.create(gamelist);

    gsheets.getWorksheet('1SUBh01AROE_wl1smHEMklEWd743bzul10DIfdLQcwV0', 'Nimet')
      .then(this.setGames.bind(this), err => console.error(err));
  }

  setGames(result) {
    var games = [];
    for(var index in result.data) {
      var game = result.data[index];
      games.push(game);
    }
    this.setState({ games: games, voted: false });
  }

  vote() {
    var nameinput = document.getElementById("nameinput");
    if (nameinput.value.length === 0)
    {
      alert("Sivun yläreunassa olevassa kentässä pitää olla nimi ennen kuin voit äänestää!");
      return;
    }

    var gameIdList = [];
    $('#gamelist li').each(function(index) {
      gameIdList.push($(this).data('key'));
    });

    var voteinfo = { name: nameinput.value, vote: gameIdList.join(';') };
    $.getJSON('http://146.185.155.151/_submit', voteinfo, function(data) {
      //console.log(data);
    });

    this.setState({ games: [], voted: true });
  }

  render() {
    if (this.state.voted) {
      return (
        <div className="container">
          <h1>Vastauksesi on vastaanotettu</h1>
          <p>Voit sulkea tämän sivun.</p>
        </div>
      );
    }
    var games = this.state.games;
    var gameComponentList = [];

    for (var game of games) {
      gameComponentList.push(<Game key={game.tunniste} keyProp={game.tunniste} name={game.nimi} creator={game.tekijat} imgSrc={game.kuvaurl} />);
    }

    return (
      <div className="container">
        <div className="page-header">
            <h1><img alt="" src="https://trac.cc.jyu.fi/projects/npo/attachment/wiki/Etusivu/pieni_keltainen_pacman.png?format=raw" /> Pelien äänestys <small>Viikko 31</small></h1>
        </div>
        <div className="form-group">
          <label htmlFor="nameinput">Äänestäjän nimi</label>
          <input type="text" className="form-control" id="nameinput" placeholder="Kirjoita tähän etu- ja sukunimesi." />
        </div>
        <p className="lead">Raahaa pelit paremmuusjärjestykseen siten, että paras peli on <i>ylimpänä</i>.</p>
        <ol id="gamelist" className="list-group extra" style={{marginRight: 3 + 'em'}}>
          {gameComponentList}
        </ol>
        <div class="form-group">&nbsp;</div>
        <p>Paina Äänestä-painiketta sitten kun olet laittanut pelit järjestykseen.</p>
        <div className="form-group">
          {/*<div className="col-sm-10">*/}
            <button type="submit" className="btn btn-success" onClick={this.vote.bind(this)}>
              <span className="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;ÄÄNESTÄ
            </button>
          {/*</div>*/}
        </div>
        <div class="form-group">&nbsp;</div>
      </div>
    );
  }
}

export default App;
